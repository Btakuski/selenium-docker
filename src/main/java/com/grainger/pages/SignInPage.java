package com.grainger.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignInPage {
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(id = "username")
    private WebElement usernameInput;

    @FindBy(id = "password")
    private WebElement passwordInput;

    @FindBy(id = "authenticationform-submit")
    private WebElement signInButton;

    @FindBy(className = "acct-number")
    private WebElement acctNum;

    public SignInPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(driver, this);
    }

    public void goTo() {
        this.driver.get("https://www.grainger.com/myaccount/signin?frwdUrlPath=%2F");
        this.wait.until(ExpectedConditions.visibilityOf(this.passwordInput));
    }

    public void enterUserDetails(String username, String password) {
        this.wait.until(ExpectedConditions.elementToBeClickable(this.passwordInput));
        this.usernameInput.sendKeys(username);
        this.passwordInput.sendKeys(password);
    }

    public void clickSignInButton() {
        this.wait.until(ExpectedConditions.elementToBeClickable(this.signInButton));
        this.signInButton.click();
    }

    public String getAcctNum() {
        this.wait.until(ExpectedConditions.visibilityOf(this.acctNum));
        return this.acctNum.getText();
    }
}
