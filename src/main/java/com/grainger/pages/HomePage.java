package com.grainger.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class HomePage {

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(id = "search")
    private WebElement searchBox;

    @FindBy(id = "username")
    private WebElement usernameInput;

    @FindBy(id = "password")
    private WebElement passwordInput;

    @FindBy(id = "signInLink")
    private WebElement signInLink;

    @FindBy(id = "homepageHeroContainer")
    private WebElement homepageHeroContainer;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(driver, this);
    }

    public void goTo() {
        this.driver.get("https://www.grainger.com/");
        this.wait.until(ExpectedConditions.visibilityOf(this.searchBox));
    }

    public void clickSignInLink() {
        this.signInLink.click();
    }

    public void verifyPage() {
        this.wait.until(ExpectedConditions.visibilityOf(this.homepageHeroContainer));
    }
}
