package com.grainger.tests;

import com.grainger.pages.HomePage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class HomePageTest extends BaseTest {

    @Test
    public void homePageTest() {
        HomePage homePage = new HomePage(driver);
        homePage.goTo();
        homePage.verifyPage();
    }

}
