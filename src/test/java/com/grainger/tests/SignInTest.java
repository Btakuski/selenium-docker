package com.grainger.tests;

import com.grainger.pages.HomePage;
import com.grainger.pages.SignInPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import tests.BaseTest;

public class SignInTest extends BaseTest {
    private String userName;
    private String password;
    private String expectedAcctNum;


    @Test
    @Parameters({"expectedAcctNum", "userName", "password"})
    public void signInTest(@Optional String expectedAcctNum, @Optional String userName, @Optional String password) {
        SignInPage signInPage = new SignInPage(driver);
        signInPage.goTo();
        signInPage.enterUserDetails(userName, password);
        signInPage.clickSignInButton();
        String actualAcctNum = signInPage.getAcctNum();
        Assert.assertEquals(actualAcctNum, expectedAcctNum);
        System.out.println("Result: " + expectedAcctNum.contentEquals(actualAcctNum));
    }

}
