package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseTest {
    protected WebDriver driver;

    @BeforeTest(description = "Configure something before test")
    public void setupDriver(ITestContext ctx) {

        String host = "localhost";
        DesiredCapabilities dc;

        if(System.getProperty("BROWSER") != null &&
                System.getProperty("BROWSER").equalsIgnoreCase("firefox")) {
            dc = DesiredCapabilities.firefox();
        } else {
            dc = DesiredCapabilities.chrome();
        }

        if(System.getProperty("HUB_HOST") != null) {
            host = System.getProperty("HUB_HOST");
        }

        String completeUrl = "http://" + host + ":4444/wd/hub";
        dc.setCapability("name", ctx.getCurrentXmlTest().getName());

        try {
            this.driver = new RemoteWebDriver(new URL(completeUrl), dc);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @AfterTest(description = "Quitting test gracefully")
    public void tearDown() {
        this.driver.quit();
    }
}
