FROM openjdk:8u201-jre-alpine3.9

RUN apk add curl jq

# Workspace
WORKDIR /usr/share/selenium-docker

# ADD .jars under target from host
# into this image
ADD target/selenium-docker.jar          selenium-docker.jar
ADD target/selenium-docker-tests.jar    selenium-docker-tests.jar
ADD target/libs                         libs

# ADD dependencies here

# ADD health check script
ADD healthcheck.sh                      healthcheck.sh

# ADD suite files
ADD home-page-module.xml        home-page-module.xml
ADD sign-in-module.xml          sign-in-module.xml

ENTRYPOINT sh healthcheck.sh